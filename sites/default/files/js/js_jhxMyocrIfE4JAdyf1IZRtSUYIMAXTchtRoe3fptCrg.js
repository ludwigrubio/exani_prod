/* Source and licensing information for the line(s) below can be found at http://exani.mx/core/assets/vendor/jquery.ui/ui/tabbable-min.js. */
/*! jQuery UI - v1.12.1 - 2017-03-31
* http://jqueryui.com
* Copyright jQuery Foundation and other contributors; Licensed  */
!function(a){"function"==typeof define&&define.amd?define(["jquery","./version","./focusable"],a):a(jQuery)}(function(a){return a.extend(a.expr[":"],{tabbable:function(b){var c=a.attr(b,"tabindex"),d=null!=c;return(!d||c>=0)&&a.ui.focusable(b,d)}})});
/* Source and licensing information for the above line(s) can be found at http://exani.mx/core/assets/vendor/jquery.ui/ui/tabbable-min.js. */;
/* Source and licensing information for the line(s) below can be found at http://exani.mx/core/assets/vendor/jquery.ui/ui/unique-id-min.js. */
/*! jQuery UI - v1.12.1 - 2017-03-31
* http://jqueryui.com
* Copyright jQuery Foundation and other contributors; Licensed  */
!function(a){"function"==typeof define&&define.amd?define(["jquery","./version"],a):a(jQuery)}(function(a){return a.fn.extend({uniqueId:function(){var a=0;return function(){return this.each(function(){this.id||(this.id="ui-id-"+ ++a)})}}(),removeUniqueId:function(){return this.each(function(){/^ui-id-\d+$/.test(this.id)&&a(this).removeAttr("id")})}})});
/* Source and licensing information for the above line(s) can be found at http://exani.mx/core/assets/vendor/jquery.ui/ui/unique-id-min.js. */;
/* Source and licensing information for the line(s) below can be found at http://exani.mx/core/assets/vendor/jquery.ui/ui/version-min.js. */
/*! jQuery UI - v1.12.1 - 2017-03-31
* http://jqueryui.com
* Copyright jQuery Foundation and other contributors; Licensed  */
!function(a){"function"==typeof define&&define.amd?define(["jquery"],a):a(jQuery)}(function(a){return a.ui=a.ui||{},a.ui.version="1.12.1"});
/* Source and licensing information for the above line(s) can be found at http://exani.mx/core/assets/vendor/jquery.ui/ui/version-min.js. */;
/* Source and licensing information for the line(s) below can be found at http://exani.mx/core/assets/vendor/jquery.ui/ui/focusable-min.js. */
/*! jQuery UI - v1.12.1 - 2017-03-31
* http://jqueryui.com
* Copyright jQuery Foundation and other contributors; Licensed  */
!function(a){"function"==typeof define&&define.amd?define(["jquery","./version"],a):a(jQuery)}(function(a){function b(a){for(var b=a.css("visibility");"inherit"===b;)a=a.parent(),b=a.css("visibility");return"hidden"!==b}return a.ui.focusable=function(c,d){var e,f,g,h,i,j=c.nodeName.toLowerCase();return"area"===j?(e=c.parentNode,f=e.name,!(!c.href||!f||"map"!==e.nodeName.toLowerCase())&&(g=a("img[usemap='#"+f+"']"),g.length>0&&g.is(":visible"))):(/^(input|select|textarea|button|object)$/.test(j)?(h=!c.disabled,h&&(i=a(c).closest("fieldset")[0],i&&(h=!i.disabled))):h="a"===j?c.href||d:d,h&&a(c).is(":visible")&&b(a(c)))},a.extend(a.expr[":"],{focusable:function(b){return a.ui.focusable(b,null!=a.attr(b,"tabindex"))}}),a.ui.focusable});
/* Source and licensing information for the above line(s) can be found at http://exani.mx/core/assets/vendor/jquery.ui/ui/focusable-min.js. */;
/* Source and licensing information for the line(s) below can be found at http://exani.mx/core/assets/vendor/jquery.ui/ui/ie-min.js. */
/*! jQuery UI - v1.12.1 - 2017-03-31
* http://jqueryui.com
* Copyright jQuery Foundation and other contributors; Licensed  */
!function(a){"function"==typeof define&&define.amd?define(["jquery","./version"],a):a(jQuery)}(function(a){return a.ui.ie=!!/msie [\w.]+/.exec(navigator.userAgent.toLowerCase())});
/* Source and licensing information for the above line(s) can be found at http://exani.mx/core/assets/vendor/jquery.ui/ui/ie-min.js. */;
/* Source and licensing information for the line(s) below can be found at http://exani.mx/core/assets/vendor/jquery.ui/ui/keycode-min.js. */
/*! jQuery UI - v1.12.1 - 2017-03-31
* http://jqueryui.com
* Copyright jQuery Foundation and other contributors; Licensed  */
!function(a){"function"==typeof define&&define.amd?define(["jquery","./version"],a):a(jQuery)}(function(a){return a.ui.keyCode={BACKSPACE:8,COMMA:188,DELETE:46,DOWN:40,END:35,ENTER:13,ESCAPE:27,HOME:36,LEFT:37,PAGE_DOWN:34,PAGE_UP:33,PERIOD:190,RIGHT:39,SPACE:32,TAB:9,UP:38}});
/* Source and licensing information for the above line(s) can be found at http://exani.mx/core/assets/vendor/jquery.ui/ui/keycode-min.js. */;
/* Source and licensing information for the line(s) below can be found at http://exani.mx/core/assets/vendor/jquery.ui/ui/plugin-min.js. */
/*! jQuery UI - v1.12.1 - 2017-03-31
* http://jqueryui.com
* Copyright jQuery Foundation and other contributors; Licensed  */
!function(a){"function"==typeof define&&define.amd?define(["jquery","./version"],a):a(jQuery)}(function(a){return a.ui.plugin={add:function(b,c,d){var e,f=a.ui[b].prototype;for(e in d)f.plugins[e]=f.plugins[e]||[],f.plugins[e].push([c,d[e]])},call:function(a,b,c,d){var e,f=a.plugins[b];if(f&&(d||a.element[0].parentNode&&11!==a.element[0].parentNode.nodeType))for(e=0;e<f.length;e++)a.options[f[e][0]]&&f[e][1].apply(a.element,c)}}});
/* Source and licensing information for the above line(s) can be found at http://exani.mx/core/assets/vendor/jquery.ui/ui/plugin-min.js. */;
/* Source and licensing information for the line(s) below can be found at http://exani.mx/core/assets/vendor/jquery.ui/ui/safe-active-element-min.js. */
/*! jQuery UI - v1.12.1 - 2017-03-31
* http://jqueryui.com
* Copyright jQuery Foundation and other contributors; Licensed  */
!function(a){"function"==typeof define&&define.amd?define(["jquery","./version"],a):a(jQuery)}(function(a){return a.ui.safeActiveElement=function(a){var b;try{b=a.activeElement}catch(c){b=a.body}return b||(b=a.body),b.nodeName||(b=a.body),b}});
/* Source and licensing information for the above line(s) can be found at http://exani.mx/core/assets/vendor/jquery.ui/ui/safe-active-element-min.js. */;
/* Source and licensing information for the line(s) below can be found at http://exani.mx/core/assets/vendor/jquery.ui/ui/safe-blur-min.js. */
/*! jQuery UI - v1.12.1 - 2017-03-31
* http://jqueryui.com
* Copyright jQuery Foundation and other contributors; Licensed  */
!function(a){"function"==typeof define&&define.amd?define(["jquery","./version"],a):a(jQuery)}(function(a){return a.ui.safeBlur=function(b){b&&"body"!==b.nodeName.toLowerCase()&&a(b).trigger("blur")}});
/* Source and licensing information for the above line(s) can be found at http://exani.mx/core/assets/vendor/jquery.ui/ui/safe-blur-min.js. */;
/* Source and licensing information for the line(s) below can be found at http://exani.mx/core/modules/contextual/js/contextual.js. */
(function(e,t,a,i,u,c,n){var r=e.extend(a.contextual,{strings:{open:t.t('Open'),close:t.t('Close')}});var s=n.getItem('Drupal.contextual.permissionsHash'),o=a.user.permissionsHash;if(s!==o){if(typeof o==='string'){i.chain(n).keys().each(function(e){if(e.substring(0,18)==='Drupal.contextual.'){n.removeItem(e)}})};n.setItem('Drupal.contextual.permissionsHash',o)};function l(n,o){var s=n.closest('.contextual-region'),i=t.contextual;n.html(o).addClass('contextual').prepend(t.theme('contextualTrigger'));var c='destination='+t.encodePath(a.path.currentPath);n.find('.contextual-links a').each(function(){var e=this.getAttribute('href'),t=e.indexOf('?')===-1?'?':'&';this.setAttribute('href',e+t+c)});var l=new i.StateModel({title:s.find('h2').eq(0).text().trim()});var u=e.extend({el:n,model:l},r);i.views.push({visual:new i.VisualView(u),aural:new i.AuralView(u),keyboard:new i.KeyboardView(u)});i.regionViews.push(new i.RegionView(e.extend({el:s,model:l},r)));i.collection.add(l);e(document).trigger('drupalContextualLinkAdded',{$el:n,$region:s,model:l});d(n)};function d(e){var n=e.parents('.contextual-region').eq(-1).find('.contextual');if(n.length<=1){return};var o=n.eq(0).offset().top,r=n.eq(1).offset().top;if(o===r){var t=n.eq(1),a=0,i=t.find('.trigger');i.removeClass('visually-hidden');a=t.height();i.addClass('visually-hidden');t.css({top:t.position().top+a})}};t.behaviors.contextual={attach:function(a){var r=e(a),o=r.find('[data-contextual-id]').once('contextual-render');if(o.length===0){return};var s=[];o.each(function(){s.push(e(this).attr('data-contextual-id'))});var u=i.filter(s,function(e){var t=n.getItem('Drupal.contextual.'+e);if(t&&t.length){window.setTimeout(function(){l(r.find('[data-contextual-id="'+e+'"]'),t)});return!1};return!0});if(u.length>0){e.ajax({url:t.url('contextual/render'),type:'POST',data:{'ids[]':u},dataType:'json',success:function(e){i.each(e,function(e,t){n.setItem('Drupal.contextual.'+t,e);if(e.length>0){o=r.find('[data-contextual-id="'+t+'"]');for(var a=0;a<o.length;a++){l(o.eq(a),e)}}})}})}}};t.contextual={views:[],regionViews:[]};t.contextual.collection=new u.Collection([],{model:t.contextual.StateModel});t.theme.contextualTrigger=function(){return'<button class="trigger visually-hidden focusable" type="button"></button>'};e(document).on('drupalContextualLinkAdded',function(e,n){t.ajax.bindAjaxLinks(n.$el[0])})})(jQuery,Drupal,drupalSettings,_,Backbone,window.JSON,window.sessionStorage);
/* Source and licensing information for the above line(s) can be found at http://exani.mx/core/modules/contextual/js/contextual.js. */;
/* Source and licensing information for the line(s) below can be found at http://exani.mx/core/modules/contextual/js/models/StateModel.js. */
(function(t,s){t.contextual.StateModel=s.Model.extend({defaults:{title:'',regionIsHovered:!1,hasFocus:!1,isOpen:!1,isLocked:!1},toggleOpen:function(){var t=!this.get('isOpen');this.set('isOpen',t);if(t){this.focus()};return this},close:function(){this.set('isOpen',!1);return this},focus:function(){this.set('hasFocus',!0);var t=this.cid;this.collection.each(function(s){if(s.cid!==t){s.close().blur()}});return this},blur:function(){if(!this.get('isOpen')){this.set('hasFocus',!1)};return this}})})(Drupal,Backbone);
/* Source and licensing information for the above line(s) can be found at http://exani.mx/core/modules/contextual/js/models/StateModel.js. */;
/* Source and licensing information for the line(s) below can be found at http://exani.mx/core/modules/contextual/js/views/AuralView.js. */
(function(t,i){t.contextual.AuralView=i.View.extend({initialize:function(t){this.options=t;this.listenTo(this.model,'change',this.render);this.$el.attr('role','form');this.render()},render:function(){var i=this.model.get('isOpen');this.$el.find('.contextual-links').prop('hidden',!i);this.$el.find('.trigger').text(t.t('@action @title configuration options',{'@action':!i?this.options.strings.open:this.options.strings.close,'@title':this.model.get('title')})).attr('aria-pressed',i)}})})(Drupal,Backbone);
/* Source and licensing information for the above line(s) can be found at http://exani.mx/core/modules/contextual/js/views/AuralView.js. */;
/* Source and licensing information for the line(s) below can be found at http://exani.mx/core/modules/contextual/js/views/KeyboardView.js. */
(function(t,i){t.contextual.KeyboardView=i.View.extend({events:{'focus .trigger':'focus','focus .contextual-links a':'focus','blur .trigger':function(){this.model.blur()},'blur .contextual-links a':function(){var t=this;this.timer=window.setTimeout(function(){t.model.close().blur()},150)}},initialize:function(){this.timer=NaN},focus:function(){window.clearTimeout(this.timer);this.model.focus()}})})(Drupal,Backbone);
/* Source and licensing information for the above line(s) can be found at http://exani.mx/core/modules/contextual/js/views/KeyboardView.js. */;
/* Source and licensing information for the line(s) below can be found at http://exani.mx/core/modules/contextual/js/views/RegionView.js. */
(function(e,t,n){e.contextual.RegionView=t.View.extend({events:function(){var e={mouseenter:function(){this.model.set('regionIsHovered',!0)},mouseleave:function(){this.model.close().blur().set('regionIsHovered',!1)}};if(n.touchevents){e={}};return e},initialize:function(){this.listenTo(this.model,'change:hasFocus',this.render)},render:function(){this.$el.toggleClass('focus',this.model.get('hasFocus'));return this}})})(Drupal,Backbone,Modernizr);
/* Source and licensing information for the above line(s) can be found at http://exani.mx/core/modules/contextual/js/views/RegionView.js. */;
/* Source and licensing information for the line(s) below can be found at http://exani.mx/core/modules/contextual/js/views/VisualView.js. */
(function(e,t,i){e.contextual.VisualView=t.View.extend({events:function(){var e=function(e){e.preventDefault();e.target.click()},t={'click .trigger':function(){this.model.toggleOpen()},'touchend .trigger':e,'click .contextual-links a':function(){this.model.close().blur()},'touchend .contextual-links a':e};if(!i.touchevents){t.mouseenter=function(){this.model.focus()}};return t},initialize:function(){this.listenTo(this.model,'change',this.render)},render:function(){var e=this.model.get('isOpen'),t=this.model.get('isLocked')||this.model.get('regionIsHovered')||e;this.$el.toggleClass('open',e).find('.trigger').toggleClass('visually-hidden',!t);if('isOpen' in this.model.changed){this.$el.closest('.contextual-region').find('.contextual .trigger:not(:first)').toggle(!e)};return this}})})(Drupal,Backbone,Modernizr);
/* Source and licensing information for the above line(s) can be found at http://exani.mx/core/modules/contextual/js/views/VisualView.js. */;
