/* Source and licensing information for the line(s) below can be found at http://exani.mx/modules/contrib/commerce/js/conditions.js. */
(function(t,n,e){'use strict';e.behaviors.conditionSummary={attach:function(){t('.vertical-tabs__pane').each(function(){t(this).drupalSetSummary(function(n){if(t(n).find('input.enable:checked').length){return e.t('Restricted')}
else{return e.t('Not restricted')}})})}}})(jQuery,window,Drupal);
/* Source and licensing information for the above line(s) can be found at http://exani.mx/modules/contrib/commerce/js/conditions.js. */