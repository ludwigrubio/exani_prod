<?php

/* themes/contrib/exanitrap/templates/system/page.html.twig */
class __TwigTemplate_a6ca9e040c10e559f5a908536a10a0607f560c6c33405292bc3f6df7cd8be3d7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'navbar' => array($this, 'block_navbar'),
            'banner' => array($this, 'block_banner'),
            'floor_one' => array($this, 'block_floor_one'),
            'floor_two' => array($this, 'block_floor_two'),
            'floor_three' => array($this, 'block_floor_three'),
            'floor_four' => array($this, 'block_floor_four'),
            'main' => array($this, 'block_main'),
            'header' => array($this, 'block_header'),
            'sidebar_first' => array($this, 'block_sidebar_first'),
            'highlighted' => array($this, 'block_highlighted'),
            'help' => array($this, 'block_help'),
            'content' => array($this, 'block_content'),
            'sidebar_second' => array($this, 'block_sidebar_second'),
            'pre_footer' => array($this, 'block_pre_footer'),
            'footer' => array($this, 'block_footer'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("set" => 54, "if" => 56, "block" => 57);
        $filters = array("clean_class" => 62, "t" => 74);
        $functions = array();

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array('set', 'if', 'block'),
                array('clean_class', 't'),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 54
        $context["container"] = (($this->getAttribute($this->getAttribute(($context["theme"] ?? null), "settings", array()), "fluid_container", array())) ? ("container-fluid") : ("container"));
        // line 56
        if (($this->getAttribute(($context["page"] ?? null), "navigation", array()) || $this->getAttribute(($context["page"] ?? null), "navigation_collapsible", array()))) {
            // line 57
            echo "  ";
            $this->displayBlock('navbar', $context, $blocks);
        }
        // line 94
        echo "
";
        // line 96
        if ($this->getAttribute(($context["page"] ?? null), "banner", array())) {
            // line 97
            echo "  ";
            $this->displayBlock('banner', $context, $blocks);
        }
        // line 103
        echo "
";
        // line 105
        if ($this->getAttribute(($context["page"] ?? null), "floor_one", array())) {
            // line 106
            echo "  ";
            $this->displayBlock('floor_one', $context, $blocks);
        }
        // line 112
        echo "
";
        // line 114
        if ($this->getAttribute(($context["page"] ?? null), "floor_two", array())) {
            // line 115
            echo "  ";
            $this->displayBlock('floor_two', $context, $blocks);
        }
        // line 121
        echo "
";
        // line 123
        if ($this->getAttribute(($context["page"] ?? null), "floor_three", array())) {
            // line 124
            echo "  ";
            $this->displayBlock('floor_three', $context, $blocks);
        }
        // line 130
        echo "
";
        // line 132
        if ($this->getAttribute(($context["page"] ?? null), "floor_four", array())) {
            // line 133
            echo "  ";
            $this->displayBlock('floor_four', $context, $blocks);
        }
        // line 139
        echo "
";
        // line 141
        $this->displayBlock('main', $context, $blocks);
        // line 206
        echo "
";
        // line 207
        if ($this->getAttribute(($context["page"] ?? null), "pre_footer", array())) {
            // line 208
            echo "  ";
            $this->displayBlock('pre_footer', $context, $blocks);
        }
        // line 214
        echo "
";
        // line 215
        if ($this->getAttribute(($context["page"] ?? null), "footer", array())) {
            // line 216
            echo "  ";
            $this->displayBlock('footer', $context, $blocks);
        }
    }

    // line 57
    public function block_navbar($context, array $blocks = array())
    {
        // line 58
        echo "    ";
        // line 59
        $context["navbar_classes"] = array(0 => "navbar", 1 => (($this->getAttribute($this->getAttribute(        // line 61
($context["theme"] ?? null), "settings", array()), "navbar_inverse", array())) ? ("navbar-inverse") : ("navbar-default")), 2 => (($this->getAttribute($this->getAttribute(        // line 62
($context["theme"] ?? null), "settings", array()), "navbar_position", array())) ? (("navbar-" . \Drupal\Component\Utility\Html::getClass($this->getAttribute($this->getAttribute(($context["theme"] ?? null), "settings", array()), "navbar_position", array())))) : (($context["container"] ?? null))));
        // line 65
        echo "    <header";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["navbar_attributes"] ?? null), "addClass", array(0 => ($context["navbar_classes"] ?? null)), "method"), "html", null, true));
        echo " id=\"navbar\" role=\"banner\">
      ";
        // line 66
        if ( !$this->getAttribute(($context["navbar_attributes"] ?? null), "hasClass", array(0 => ($context["container"] ?? null)), "method")) {
            // line 67
            echo "        <div class=\"";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["container"] ?? null), "html", null, true));
            echo "\">
      ";
        }
        // line 69
        echo "      <div class=\"navbar-header\">
        ";
        // line 70
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "navigation", array()), "html", null, true));
        echo "
        ";
        // line 72
        echo "        ";
        if ($this->getAttribute(($context["page"] ?? null), "navigation_collapsible", array())) {
            // line 73
            echo "          <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#navbar-collapse\">
            <span class=\"sr-only\">";
            // line 74
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Toggle navigation")));
            echo "</span>
            <span class=\"icon-bar\"></span>
            <span class=\"icon-bar\"></span>
            <span class=\"icon-bar\"></span>
          </button>
        ";
        }
        // line 80
        echo "      </div>

      ";
        // line 83
        echo "      ";
        if ($this->getAttribute(($context["page"] ?? null), "navigation_collapsible", array())) {
            // line 84
            echo "        <div id=\"navbar-collapse\" class=\"navbar-collapse collapse\">
          ";
            // line 85
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "navigation_collapsible", array()), "html", null, true));
            echo "
        </div>
      ";
        }
        // line 88
        echo "      ";
        if ( !$this->getAttribute(($context["navbar_attributes"] ?? null), "hasClass", array(0 => ($context["container"] ?? null)), "method")) {
            // line 89
            echo "        </div>
      ";
        }
        // line 91
        echo "    </header>
  ";
    }

    // line 97
    public function block_banner($context, array $blocks = array())
    {
        // line 98
        echo "    <div class=\"banner\" role=\"contentinfo\">
      ";
        // line 99
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "banner", array()), "html", null, true));
        echo "
    </div>
  ";
    }

    // line 106
    public function block_floor_one($context, array $blocks = array())
    {
        // line 107
        echo "    <div class=\"floor_one\" role=\"contentinfo\">
      ";
        // line 108
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "floor_one", array()), "html", null, true));
        echo "
    </div>
  ";
    }

    // line 115
    public function block_floor_two($context, array $blocks = array())
    {
        // line 116
        echo "    <div class=\"floor_two\" role=\"contentinfo\">
      ";
        // line 117
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "floor_two", array()), "html", null, true));
        echo "
    </div>
  ";
    }

    // line 124
    public function block_floor_three($context, array $blocks = array())
    {
        // line 125
        echo "    <div class=\"floor_three\" role=\"contentinfo\">
      ";
        // line 126
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "floor_three", array()), "html", null, true));
        echo "
    </div>
  ";
    }

    // line 133
    public function block_floor_four($context, array $blocks = array())
    {
        // line 134
        echo "    <div class=\"floor_four\" role=\"contentinfo\">
      ";
        // line 135
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "floor_four", array()), "html", null, true));
        echo "
    </div>
  ";
    }

    // line 141
    public function block_main($context, array $blocks = array())
    {
        // line 142
        echo "  <div role=\"main\" class=\"main-container ";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["container"] ?? null), "html", null, true));
        echo " js-quickedit-main-content\">
    <div class=\"row\">

      ";
        // line 146
        echo "      ";
        if ($this->getAttribute(($context["page"] ?? null), "header", array())) {
            // line 147
            echo "        ";
            $this->displayBlock('header', $context, $blocks);
            // line 152
            echo "      ";
        }
        // line 153
        echo "
      ";
        // line 155
        echo "      ";
        if ($this->getAttribute(($context["page"] ?? null), "sidebar_first", array())) {
            // line 156
            echo "        ";
            $this->displayBlock('sidebar_first', $context, $blocks);
            // line 161
            echo "      ";
        }
        // line 162
        echo "
      ";
        // line 164
        echo "      ";
        // line 165
        $context["content_classes"] = array(0 => ((($this->getAttribute(        // line 166
($context["page"] ?? null), "sidebar_first", array()) && $this->getAttribute(($context["page"] ?? null), "sidebar_second", array()))) ? ("col-sm-6") : ("")), 1 => ((($this->getAttribute(        // line 167
($context["page"] ?? null), "sidebar_first", array()) && twig_test_empty($this->getAttribute(($context["page"] ?? null), "sidebar_second", array())))) ? ("col-sm-9") : ("")), 2 => ((($this->getAttribute(        // line 168
($context["page"] ?? null), "sidebar_second", array()) && twig_test_empty($this->getAttribute(($context["page"] ?? null), "sidebar_first", array())))) ? ("col-sm-9") : ("")), 3 => (((twig_test_empty($this->getAttribute(        // line 169
($context["page"] ?? null), "sidebar_first", array())) && twig_test_empty($this->getAttribute(($context["page"] ?? null), "sidebar_second", array())))) ? ("col-sm-12") : ("")));
        // line 172
        echo "      <section";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content_attributes"] ?? null), "addClass", array(0 => ($context["content_classes"] ?? null)), "method"), "html", null, true));
        echo ">

        ";
        // line 175
        echo "        ";
        if ($this->getAttribute(($context["page"] ?? null), "highlighted", array())) {
            // line 176
            echo "          ";
            $this->displayBlock('highlighted', $context, $blocks);
            // line 179
            echo "        ";
        }
        // line 180
        echo "
        ";
        // line 182
        echo "        ";
        if ($this->getAttribute(($context["page"] ?? null), "help", array())) {
            // line 183
            echo "          ";
            $this->displayBlock('help', $context, $blocks);
            // line 186
            echo "        ";
        }
        // line 187
        echo "
        ";
        // line 189
        echo "        ";
        $this->displayBlock('content', $context, $blocks);
        // line 193
        echo "      </section>

      ";
        // line 196
        echo "      ";
        if ($this->getAttribute(($context["page"] ?? null), "sidebar_second", array())) {
            // line 197
            echo "        ";
            $this->displayBlock('sidebar_second', $context, $blocks);
            // line 202
            echo "      ";
        }
        // line 203
        echo "    </div>
  </div>
";
    }

    // line 147
    public function block_header($context, array $blocks = array())
    {
        // line 148
        echo "          <div class=\"col-sm-12\" role=\"heading\">
            ";
        // line 149
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "header", array()), "html", null, true));
        echo "
          </div>
        ";
    }

    // line 156
    public function block_sidebar_first($context, array $blocks = array())
    {
        // line 157
        echo "          <aside class=\"col-sm-3\" role=\"complementary\">
            ";
        // line 158
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "sidebar_first", array()), "html", null, true));
        echo "
          </aside>
        ";
    }

    // line 176
    public function block_highlighted($context, array $blocks = array())
    {
        // line 177
        echo "            <div class=\"highlighted\">";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "highlighted", array()), "html", null, true));
        echo "</div>
          ";
    }

    // line 183
    public function block_help($context, array $blocks = array())
    {
        // line 184
        echo "            ";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "help", array()), "html", null, true));
        echo "
          ";
    }

    // line 189
    public function block_content($context, array $blocks = array())
    {
        // line 190
        echo "          <a id=\"main-content\"></a>
          ";
        // line 191
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "content", array()), "html", null, true));
        echo "
        ";
    }

    // line 197
    public function block_sidebar_second($context, array $blocks = array())
    {
        // line 198
        echo "          <aside class=\"col-sm-3\" role=\"complementary\">
            ";
        // line 199
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "sidebar_second", array()), "html", null, true));
        echo "
          </aside>
        ";
    }

    // line 208
    public function block_pre_footer($context, array $blocks = array())
    {
        // line 209
        echo "    <div class=\"pre-footer\" role=\"contentinfo\">
      ";
        // line 210
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "pre_footer", array()), "html", null, true));
        echo "
    </div>
  ";
    }

    // line 216
    public function block_footer($context, array $blocks = array())
    {
        // line 217
        echo "    <footer class=\"footer ";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["container"] ?? null), "html", null, true));
        echo "\" role=\"contentinfo\">
      ";
        // line 218
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "footer", array()), "html", null, true));
        echo "
    </footer>
  ";
    }

    public function getTemplateName()
    {
        return "themes/contrib/exanitrap/templates/system/page.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  459 => 218,  454 => 217,  451 => 216,  444 => 210,  441 => 209,  438 => 208,  431 => 199,  428 => 198,  425 => 197,  419 => 191,  416 => 190,  413 => 189,  406 => 184,  403 => 183,  396 => 177,  393 => 176,  386 => 158,  383 => 157,  380 => 156,  373 => 149,  370 => 148,  367 => 147,  361 => 203,  358 => 202,  355 => 197,  352 => 196,  348 => 193,  345 => 189,  342 => 187,  339 => 186,  336 => 183,  333 => 182,  330 => 180,  327 => 179,  324 => 176,  321 => 175,  315 => 172,  313 => 169,  312 => 168,  311 => 167,  310 => 166,  309 => 165,  307 => 164,  304 => 162,  301 => 161,  298 => 156,  295 => 155,  292 => 153,  289 => 152,  286 => 147,  283 => 146,  276 => 142,  273 => 141,  266 => 135,  263 => 134,  260 => 133,  253 => 126,  250 => 125,  247 => 124,  240 => 117,  237 => 116,  234 => 115,  227 => 108,  224 => 107,  221 => 106,  214 => 99,  211 => 98,  208 => 97,  203 => 91,  199 => 89,  196 => 88,  190 => 85,  187 => 84,  184 => 83,  180 => 80,  171 => 74,  168 => 73,  165 => 72,  161 => 70,  158 => 69,  152 => 67,  150 => 66,  145 => 65,  143 => 62,  142 => 61,  141 => 59,  139 => 58,  136 => 57,  130 => 216,  128 => 215,  125 => 214,  121 => 208,  119 => 207,  116 => 206,  114 => 141,  111 => 139,  107 => 133,  105 => 132,  102 => 130,  98 => 124,  96 => 123,  93 => 121,  89 => 115,  87 => 114,  84 => 112,  80 => 106,  78 => 105,  75 => 103,  71 => 97,  69 => 96,  66 => 94,  62 => 57,  60 => 56,  58 => 54,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#
/**
 * @file
 * Default theme implementation to display a single page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.html.twig template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - base_path: The base URL path of the Drupal installation. Will usually be
 *   \"/\" unless you have installed Drupal in a sub-directory.
 * - is_front: A flag indicating if the current page is the front page.
 * - logged_in: A flag indicating if the user is registered and signed in.
 * - is_admin: A flag indicating if the user has permission to access
 *   administration pages.
 *
 * Site identity:
 * - front_page: The URL of the front page. Use this instead of base_path when
 *   linking to the front page. This includes the language domain or prefix.
 *
 * Page content (in order of occurrence in the default page.html.twig):
 * - title_prefix: Additional output populated by modules, intended to be
 *   displayed in front of the main title tag that appears in the template.
 * - title: The page title, for use in the actual content.
 * - title_suffix: Additional output populated by modules, intended to be
 *   displayed after the main title tag that appears in the template.
 * - messages: Status and error messages. Should be displayed prominently.
 * - tabs: Tabs linking to any sub-pages beneath the current page (e.g., the
 *   view and edit tabs when displaying a node).
 * - node: Fully loaded node, if there is an automatically-loaded node
 *   associated with the page and the node ID is the second argument in the
 *   page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - page.header: Items for the header region.
 * - page.navigation: Items for the navigation region.
 * - page.navigation_collapsible: Items for the navigation (collapsible) region.
 * - page.highlighted: Items for the highlighted content region.
 * - page.help: Dynamic help text, mostly for admin pages.
 * - page.content: The main content of the current page.
 * - page.sidebar_first: Items for the first sidebar.
 * - page.sidebar_second: Items for the second sidebar.
 * - page.footer: Items for the footer region.
 *
 * @ingroup templates
 *
 * @see template_preprocess_page()
 * @see html.html.twig
 */
#}
{% set container = theme.settings.fluid_container ? 'container-fluid' : 'container' %}
{# Navbar #}
{% if page.navigation or page.navigation_collapsible %}
  {% block navbar %}
    {%
      set navbar_classes = [
        'navbar',
        theme.settings.navbar_inverse ? 'navbar-inverse' : 'navbar-default',
        theme.settings.navbar_position ? 'navbar-' ~ theme.settings.navbar_position|clean_class : container,
      ]
    %}
    <header{{ navbar_attributes.addClass(navbar_classes) }} id=\"navbar\" role=\"banner\">
      {% if not navbar_attributes.hasClass(container) %}
        <div class=\"{{ container }}\">
      {% endif %}
      <div class=\"navbar-header\">
        {{ page.navigation }}
        {# .btn-navbar is used as the toggle for collapsed navbar content #}
        {% if page.navigation_collapsible %}
          <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#navbar-collapse\">
            <span class=\"sr-only\">{{ 'Toggle navigation'|t }}</span>
            <span class=\"icon-bar\"></span>
            <span class=\"icon-bar\"></span>
            <span class=\"icon-bar\"></span>
          </button>
        {% endif %}
      </div>

      {# Navigation (collapsible) #}
      {% if page.navigation_collapsible %}
        <div id=\"navbar-collapse\" class=\"navbar-collapse collapse\">
          {{ page.navigation_collapsible }}
        </div>
      {% endif %}
      {% if not navbar_attributes.hasClass(container) %}
        </div>
      {% endif %}
    </header>
  {% endblock %}
{% endif %}

{# Banner #}
{% if page.banner %}
  {% block banner %}
    <div class=\"banner\" role=\"contentinfo\">
      {{ page.banner }}
    </div>
  {% endblock %}
{% endif %}

{# Floor 1 #}
{% if page.floor_one %}
  {% block floor_one %}
    <div class=\"floor_one\" role=\"contentinfo\">
      {{ page.floor_one }}
    </div>
  {% endblock %}
{% endif %}

{# Floor 2 #}
{% if page.floor_two %}
  {% block floor_two %}
    <div class=\"floor_two\" role=\"contentinfo\">
      {{ page.floor_two }}
    </div>
  {% endblock %}
{% endif %}

{# Floor 3 #}
{% if page.floor_three %}
  {% block floor_three %}
    <div class=\"floor_three\" role=\"contentinfo\">
      {{ page.floor_three }}
    </div>
  {% endblock %}
{% endif %}

{# Floor 4 #}
{% if page.floor_four %}
  {% block floor_four %}
    <div class=\"floor_four\" role=\"contentinfo\">
      {{ page.floor_four }}
    </div>
  {% endblock %}
{% endif %}

{# Main #}
{% block main %}
  <div role=\"main\" class=\"main-container {{ container }} js-quickedit-main-content\">
    <div class=\"row\">

      {# Header #}
      {% if page.header %}
        {% block header %}
          <div class=\"col-sm-12\" role=\"heading\">
            {{ page.header }}
          </div>
        {% endblock %}
      {% endif %}

      {# Sidebar First #}
      {% if page.sidebar_first %}
        {% block sidebar_first %}
          <aside class=\"col-sm-3\" role=\"complementary\">
            {{ page.sidebar_first }}
          </aside>
        {% endblock %}
      {% endif %}

      {# Content #}
      {%
        set content_classes = [
          page.sidebar_first and page.sidebar_second ? 'col-sm-6',
          page.sidebar_first and page.sidebar_second is empty ? 'col-sm-9',
          page.sidebar_second and page.sidebar_first is empty ? 'col-sm-9',
          page.sidebar_first is empty and page.sidebar_second is empty ? 'col-sm-12'
        ]
      %}
      <section{{ content_attributes.addClass(content_classes) }}>

        {# Highlighted #}
        {% if page.highlighted %}
          {% block highlighted %}
            <div class=\"highlighted\">{{ page.highlighted }}</div>
          {% endblock %}
        {% endif %}

        {# Help #}
        {% if page.help %}
          {% block help %}
            {{ page.help }}
          {% endblock %}
        {% endif %}

        {# Content #}
        {% block content %}
          <a id=\"main-content\"></a>
          {{ page.content }}
        {% endblock %}
      </section>

      {# Sidebar Second #}
      {% if page.sidebar_second %}
        {% block sidebar_second %}
          <aside class=\"col-sm-3\" role=\"complementary\">
            {{ page.sidebar_second }}
          </aside>
        {% endblock %}
      {% endif %}
    </div>
  </div>
{% endblock %}

{% if page.pre_footer %}
  {% block pre_footer %}
    <div class=\"pre-footer\" role=\"contentinfo\">
      {{ page.pre_footer }}
    </div>
  {% endblock %}
{% endif %}

{% if page.footer %}
  {% block footer %}
    <footer class=\"footer {{ container }}\" role=\"contentinfo\">
      {{ page.footer }}
    </footer>
  {% endblock %}
{% endif %}", "themes/contrib/exanitrap/templates/system/page.html.twig", "/home/exani/domains/exani.mx/public_html/themes/contrib/exanitrap/templates/system/page.html.twig");
    }
}
