<?php

/* modules/contrib/commerce/modules/price/templates/commerce-price-plain.html.twig */
class __TwigTemplate_fedd72e3cee35702ac7d47a6960a4893bc16e63f04b606fe9d3e3ae0dd6d4750 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array();
        $filters = array("number_format" => 14);
        $functions = array();

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array(),
                array('number_format'),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 14
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, twig_number_format_filter($this->env, ($context["number"] ?? null), 2, ".", ","), "html", null, true));
        echo " ";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["currency"] ?? null), "currencyCode", array()), "html", null, true));
        echo "
";
    }

    public function getTemplateName()
    {
        return "modules/contrib/commerce/modules/price/templates/commerce-price-plain.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  43 => 14,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#
/**
 * @file
 * Default theme implementation for displaying a price.
 *
 * Used by the 'commerce_price_plain' formatter.
 * Available variables:
 *   - number: The number.
 *   - currency: The currency entity.
 *
 * @ingroup themeable
 */
#}
{{ number|number_format(2, '.', ',') }} {{ currency.currencyCode }}
", "modules/contrib/commerce/modules/price/templates/commerce-price-plain.html.twig", "/home/exani/domains/exani.mx/public_html/modules/contrib/commerce/modules/price/templates/commerce-price-plain.html.twig");
    }
}
